from FaimlyTree.modules import Person
import cv2
from matplotlib import pyplot as plt

person1 = Person("Faisal", "Alyousefi", "Denver")
person2 = Person("Khaled", "Alyousefi", "Arar")

print(person1.fname + " " + person1.lname + ", From:" + person1.city)
print(person2.fname + " " + person2.lname + ", From:" + person2.city)

image1 = cv2.imread('arraw.png', 1)

cv2.imshow('Showing image', image1)
cv2.waitKey(0)
cv2.destroyAllWindows()

plt.imshow(image1, cmap='gray', interpolation='bicubic')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
plt.show()
