s = "faisal is that me."
print (s)
# upper case
print (s.upper())
print ("faisal".upper())
# format
print ("faisal hi {} ".format("not know"))
# capitalize
print (s.capitalize())
# swap
print ("Hello is World".swapcase())
# find
print (s.find('is'))
 # replace
print (s.replace('faisal','world'))
print(s)
print (id(s))
# new string
w = s.upper()
print (w)
# strip
print (s.strip())
print ('       faisal       '.strip())
# isalnum
print (s.isalnum())

c,d = 23,45
# format
print(c,d)
print('this is {},that is {}'.format(c,d))
print('this is {},that is {}'.format(d,c))
print("this is {1} this is {0} this is {1}".format(c,d))
# split
print(s.split())
print(s.split('i'))
word = s.split()
print(word)
for w in word: print(w)
new = ':'.join(word)
print(new)
# center
new = s.center(85)
print(new)


