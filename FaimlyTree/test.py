from modules import *

faisal = Person("Faisal", "Alyousefi", Birthday(2004, 5, 24), "Denver", 'm')
nora = Person("Nora", "Alyousefi", Birthday(2010, 8, 9), "Riyadh", 'f')
suleman = Person("Suleman", "Alyousefi", Birthday(2009, 5, 7), "Arar", 'm')
dana = Person("Dana", "Alyousefi", Birthday(2016, 8, 23), "Colorado", 'f')
nuha = Person("Nuha", "Alaqeel", Birthday(2009, 5, 7), "Arar", 'f')
khaled = Person("Khaled", "Alyousefi", Birthday(1977, 8, 2), "Arar", 'm')

family1 = Family(nuha, khaled)
print(family1.name)


family1.add_members([nora])
family1.printMembersName()

print(khaled.numberofDependant())
print(nora.mother.name)