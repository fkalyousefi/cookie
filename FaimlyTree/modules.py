from datetime import datetime

############ Person Class ############
class Person:
    def __init__(self, fname, lname, birthday, city, gender):
        self.fname = fname
        self.lname = lname
        self.name  = fname + ' ' + lname
        self.birthday = birthday
        self.city = city
        self.gender = gender

    def getFullName(self):
        return (self.fname + " " + self.lname)

    def makeParent(self):
        self.__class__ = Parnet
        self.add_parent_data()

############ Parent Class ############
class Parnet(Person):
    def __init__(self):
        self.add_parent_data()

    def add_parent_data(self):
        self.children = []

        # Add Mrs. or Mr. to the name
        # if self.gender == 'f':
        #     self.name = "Mrs. " + super().getFullName()
        # elif self.gender == 'm':
        #     self.name = "Mr. " + super().getFullName()

    def getFullName(self):
        if self.gender == 'f':
            return "Mrs. " + super().getFullName()
        elif self.gender == 'm':
            return "Mr. " + super().getFullName()

    def numberofDependant(self):
        doughters, sons = 0, 0
        for i in range(0, len(self.children)):
            if self.children[i].gender == 'f':
                doughters += 1
            elif self.children[i].gender == 'm':
                sons += 1

        return doughters, sons

############ Family Class ############
class Family:
    def __init__(self, mother, father, *members):
        self.name = (mother.getFullName() + " & " + father.getFullName() + " Family")
        mother.makeParent()
        father.makeParent()
        self.mother = mother
        self.father = father
        self.members = []
        if members:
            self.add_members(members[0])

    def add_members(self, *members):
        members = members[0]  # take the list from the tuple
        self.members.extend(members)
        self.mother.children.extend(members)
        self.father.children.extend(members)

        for person in members:
            person.father = self.father
            person.mother = self.mother

    def printMembersName(self):
        for member in self.members:
            print('{} {}: {}'.format(member.fname, member.lname,
                                     member.birthday.getAge()))
        # for i in range(0, len(self.members)):
        #     print('{} {}, {}'.format(self.members[i].fname, self.members[i].lname,
        #                              self.members[i].birthday.getFullBirthday()))

############ Birthday Class ############
class Birthday:
    def __init__(self, year, month, day):
        self.birthday = datetime(year, month, day)

    def getBirthday(self):
        return self.birthday.strftime("%m, %d %Y")

    def getDay(self):
        return self.birthday.strftime("%A")

    def getFullBirthday(self):
        return self.getDay() + " " + self.getBirthday()

    def getNumberOfDays(self):
        today = datetime.now()
        return (today - self.birthday).days

    def getAge(self):
        today = datetime.now()
        years = today.year - self.birthday.year - ((today.month, today.day) < (self.birthday.month, self.birthday.day))
        months = (today.month - self.birthday.month - (today.day < self.birthday.day)) % 12
        days = abs(today.day - self.birthday.day)
        return (str(years) + " years, " + str(months) + " months, and " + str(days) + " days")



