import math

class Rectangle:
    def __init__(self,length,width):
        self.length = length
        self.width = width
        
    def getArea(self):
        return (self.length * self.width)


class Circle:
    def __init__(self,raduis):
        self.raduis = raduis       

    def getArea(self):
        return (math.pi  *  self.raduis **2)   

class Prism:
    def __init__(self,length, height, width):
        self.length = length
        self.height = height
        self.width  = width

    def getVolume(self):
        return (self.length * self.height * self.width)

    def isCube(self):
        if (self.length == self.height and self.length == self.width):
            return True
        else:
            return False

    
