class Product:
    def __init__(self,name,catgory,price):
        self.__name = name
        self.__catgory = catgory
        self.__price = price

    def getPrice(self):
        return self.__price
    
    def productInformation(self):
        return (self.__name +" " +self.__catgory+ " "+ str(self.__price))
 
    def changePrice(self,newPrice):
         self.__price = newPrice

    
         
def main():
    product1 = Product("Del","computer",1000)
    product2 = Product('takis','chips',5)
    product3 = Product('cola','drink',2)
    product4 = Product('bose','headphones',300)
    product5 = Product('fifa','games',60)

    print (product4.productInformation())

    product4.changePrice(product4.getPrice() * 1.10)

    print (product4.productInformation())
    
    print (product1.productInformation())
    product1.changePrice(1200)
    print (product1.productInformation())
main()
