from datetime import datetime

class Student:
    def __init__(self,name,hight,weight,birthday):
        self.name = name
        self.hight = hight
        self.weight = weight
        self.birthday = birthday

    def name(self):
        return (self.name)

    def hight(self):
        return (self.hight)

    def weight(self):
        return (self.weight)

    def changeHight(self,newHight):
        self.hight = newHight

    def changeName(self,newName):
        self.name = newName

    def getAge(self):
        return self.birthday.getAge()


class Birthday:
    def __init__(self,year,month,day):
        self.birthday = datetime(year,month,day)

    def getBirthday(self):
        return self.birthday.strftime("%m, %d %Y")

    def getDay(self):
        return self.birthday.strftime("%A")

    def getFullBirthday(self):
        return self.getDay() + " " + self.getBirthday()

    def getNumberOfDays(self):
        today = datetime.now()
        return (today-self.birthday).days

    def getAge(self):
        today = datetime.now()
        years = today.year - self.birthday.year - ((today.month, today.day) < (self.birthday.month, self.birthday.day))
        months = (today.month - self.birthday.month - (today.day < self.birthday.day)) % 12
        days = abs(today.uday - self.birthday.day)
        return (str(years)+" years, "+str(months)+" months, and "+str(days)+" days")



class Subject:
    def __init__(self,code,name,teacher):
        self.code = code
        self.name = name
        self.teacher = teacher

    def getFullDetails(self):
        return ((self.code) + ": " + (self.name) + " (" + (self.teacher)+ ")")

    def changeTeacher(self,newTeacher):
        self.teacher = newTeacher






