from Classes.School import Student
from Classes.School import Birthday
from Classes.FaisalMath import *


## SCHOOL
faisal_birthday = Birthday(2004 ,5 ,24)
khaled_birthday = Birthday(1977 ,8 ,28)

print ("Faisal: " + faisal_birthday.getBirthday())
print ("Faisal: " + faisal_birthday.getDay())
print ("Faisal: " + faisal_birthday.getFullBirthday())
print ("Khaled: " + khaled_birthday.getFullBirthday())
print ("Faisal: " + str(faisal_birthday.getNumberOfDays()))

print (Birthday(2004 ,5 ,24).getDay())

student_1 = Student("Faisal", 157, 85)
student_2 = Student("Suleman", 130, 50)

print(student_1.name() + ", " + str(student_1.hight()))

student_1.changeHight(165)

print(student_1.name() + ", " + str(student_1.hight()))

student_1.changeName("FK")

print(student_1.name() + ", " + str(student_1.hight()))

## FAISAL MATH

circle1 = Circle(2)
circle2 = Circle(5)
circle3 = Circle(54)
print(circle1.getArea())

rectangle1 = Rectangle(12, 5)
rectangle2 = Rectangle(23, 7)
rectangle3 = Rectangle(15, 9)
print(rectangle1.getArea())